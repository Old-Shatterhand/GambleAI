import numpy as np
import pandas as pd
from prettytable import PrettyTable

from classification.goal_for import GoalForrestModel
from classification.goal_net import GoalNetModel
from classification.goal_reg import GoalRegModel
from preprocessing.LSPCreator import parse_result


class Presenter:
    def __init__(self, mode, home_team, away_team):
        self.mode = mode
        self.t = PrettyTable()
        self.t.title = home_team + " - " + away_team
        h_short, a_short = home_team[:3].upper(), away_team[:3].upper()
        self.t.field_names = ["Predictor"] + \
                             ([h_short + " 0", h_short + " 1", h_short + " 2", h_short + " 3", h_short + " 4+",
                               a_short + " 0", a_short + " 1", a_short + " 2", a_short + " 3", a_short + " 4+",
                               "Result"] if mode == "goals" else ["+-", "=", "-+", "?", "Result"])

    def add_prediction(self, name, predictions):
        if self.mode == "goals":
            result = str(np.argmax(predictions[:5])) + " : " + str(np.argmax(predictions[5:]))
        else:
            result = {0: "+ -", 1: "=", 2: "- +", 3: "?"}[np.argmax(predictions)]
        self.t.add_row([name] + ["%.3f" % p for p in predictions] + [result])

    def __str__(self):
        return str(self.t)


def generate_input(home_team, away_team):
    df = pd.read_csv("./data/Goals/LSP_data_05.csv")

    data_row = [0 for _ in range(30)]
    home_series = (df[:]["home_team"] == home_team) | (df[:]["away_team"] == home_team)

    for i, hindex in enumerate([i for i in home_series.index if home_series[i]][:5]):
        data_row[i * 2:(i + 1) * 2] = \
            parse_result(df.at[hindex, "home_score"], df.at[hindex, "away_score"],
                         df.at[hindex, "home_team"] == home_team)

    both_series = (((df[:]["home_team"] == home_team) & (df[:]["away_team"] == away_team)) |
                   ((df[:]["home_team"] == away_team) & (df[:]["away_team"] == home_team)))
    for i, hindex in enumerate([i for i in both_series.index if both_series[i]][:5]):
        data_row[(i + 5) * 2:(i + 6) * 2] = \
            parse_result(df.at[hindex, "home_score"], df.at[hindex, "away_score"],
                         df.at[hindex, "home_team"] == home_team)

    away_series = ((df[:]["home_team"] == away_team) | (df[:]["away_team"] == away_team))
    for i, hindex in enumerate([i for i in away_series.index if away_series[i]][:5]):
        data_row[(i + 10) * 2:(i + 11) * 2] = \
            parse_result(df.at[hindex, "home_score"], df.at[hindex, "away_score"],
                         df.at[hindex, "away_score"] == away_team)

    return data_row


def predict(home_team, away_team, home_name, away_name):
    data = np.expand_dims(generate_input(home_team, away_team), axis=0)
    p = Presenter("goals", home_name, away_name)

    grm = GoalRegModel().load("./models/grm.pkl")
    grm_pred = grm.predict(data)
    # print("Linear:\n\t", grm_pred)
    p.add_prediction("GRM", grm_pred.squeeze())

    gfm = GoalForrestModel(None).load("./models/gfm.pkl")
    gfm_pred = gfm.predict(data)
    # print("Forrest:\n\t", gfm_pred)
    p.add_prediction("GFM", np.concatenate(gfm_pred[0], axis=0))

    net = GoalNetModel(config={"hidden": 52, "goals": False, "mode": "big", "lr": 0.001, "bs": 16, "episodes": 20,
                               "tune": False, "path": "./models/gnm.pth"})
    # print("\nNetwork:\n\t", net.predict(data))
    print(p)


def read_teams():
    output = {}
    for line in open("./data/Goals/LSP_teams.csv").readlines()[1:]:
        parts = line.split(",")
        output[parts[0]] = int(parts[-1])
    return output


if __name__ == '__main__':
    teams = read_teams()
    print("Team playing at home:")
    home_name = "Germany"  # input()
    home_team = teams.get(home_name, -1)
    print("Team playing away:")
    away_name = "England"  # input()
    away_team = teams.get(away_name, -1)

    if home_team == -1:
        print("Home team not defined.")
    if away_team == -1:
        print("Away team not defined.")
    predict(home_team, away_team, home_name, away_name)
