from functools import partial

import numpy as np
from ray import tune
from ray.tune import CLIReporter
from ray.tune.schedulers import ASHAScheduler
from ray.tune.utils.log import Verbosity

from classification.goal_for import GoalForrestModel
from classification.goal_net import GoalNetModel
from classification.goal_reg import GoalRegModel
from classification.quote_net import QuoteNetModel
from utils.data import GoalSet, QuoteSet


def train_class_gnm(config):
    if "goals" not in config:
        config["goals"] = False
    if "episodes" not in config:
        config["episodes"] = 20
    if "tune" not in config:
        config["tune"] = True

    model = GoalNetModel(config)
    dataset = GoalSet(path="C:/Users/joere/Desktop/Dokumente/Workspace/Python/GambleAI/data/Goals/LSP_data_05.csv")
    train_data, valid_data, test_data = dataset.split()
    if config.get("spath", None) is not None:
        model.save(config["spath"])
    model.fit(train_data, valid_data)


def train_class_qnm(config):
    if "goals" not in config:
        config["goals"] = False
    if "episodes" not in config:
        config["episodes"] = 20
    if "tune" not in config:
        config["tune"] = True
    if "weight" not in config:
        config["weight"] = 1
    if "max_value" not in config:
        config["max_value"] = 1

    model = QuoteNetModel(config)
    dataset = QuoteSet(path="C:/Users/joere/Desktop/Dokumente/Workspace/Python/GambleAI/data/Quotes/BuLi_data.csv")
    train_data, valid_data, test_data = dataset.split()
    model.fit(train_data, valid_data)
    if config.get("spath", None) is not None:
        model.save(config["spath"])
    return model.test(test_data)


def train_class_grm(config):
    model = GoalRegModel()
    dataset = GoalSet(path="C:/Users/joere/Desktop/Dokumente/Workspace/Python/GambleAI/data/Goals/LSP_data_05.csv")
    train_data, valid_data, test_data = dataset.split()
    model.fit(train_data.get_arrays(classes=True), valid_data.get_arrays(classes=True))
    if config.get("spath", None) is not None:
        model.save(config["spath"])
    return model.test(test_data.get_arrays(classes=True))


def train_reg_grm(config):
    model = GoalRegModel()
    dataset = GoalSet(path="C:/Users/joere/Desktop/Dokumente/Workspace/Python/GambleAI/data/Goals/LSP_data_05.csv",
                      classification=True)
    train_data, valid_data, test_data = dataset.split()
    model.fit(train_data.get_arrays(classes=True), valid_data.get_arrays(classes=True))
    if config.get("spath", None) is not None:
        model.save(config["spath"])
    return model.test(test_data.get_arrays(classes=True))


def train_class_gfm(config):
    model = GoalForrestModel(config)
    dataset = GoalSet(path="C:/Users/joere/Desktop/Dokumente/Workspace/Python/GambleAI/data/Goals/LSP_data_05.csv")
    train_data, valid_data, test_data = dataset.split()
    model.fit(train_data.get_arrays(), valid_data.get_arrays())
    if config.get("spath", None) is not None:
        model.save(config["spath"])
    return model.test(test_data.get_arrays())


def run_ray_tune():
    configuration = {
        "hidden": tune.sample_from(lambda _: 2 * np.random.randint(8, 32)),
        "mode": tune.choice(["big", "small"]),
        "lr": tune.choice([0.01, 0.001, 0.0001, 0.00001]),
        "bs": tune.choice([8, 16, 32, 64])
    }
    scheduler = ASHAScheduler(metric="loss", mode="min", max_t=15, grace_period=1, reduction_factor=2)
    reporter = CLIReporter(metric_columns=["loss", "accuracy", "training_iteration"])
    result = tune.run(partial(train_class_qnm), resources_per_trial={"cpu": 1, "gpu": 0}, config=configuration,
                      num_samples=10,
                      scheduler=scheduler, progress_reporter=reporter, verbose=Verbosity.V1_EXPERIMENT)

    best_trial = result.get_best_trial("loss", "min", "last")
    print("Best trial config: {}".format(best_trial.config))
    print("Best trial final validation loss: {}".format(
        best_trial.last_result["loss"]))
    print("Best trial final validation accuracy: {}".format(
        best_trial.last_result["accuracy"]))


# run_ray_tune()
# train_class_qnm(config={"hidden": 52, "goals": False, "mode": "big", "weight": 1, "max_value": 1, "lr": 0.001,
#                         "bs": 16, "episodes": 20, "tune": False, "path": None})
train_class_gnm(config={"hidden": 52, "goals": False, "mode": "big", "lr": 0.001, "bs": 16, "episodes": 20,
                        "tune": False, "path": None, "spath": "gnm.pth"})
print(train_class_grm({"spath": "./models/grm.pkl"}))
# print(train_class_gfm(None))
