import pandas as pd


def parse_result(home, away, home_first=True):
    if home_first:
        return [home, away]
    return [away, home]


def find_data_row(df, row, teams, index=0, length=4):
    if row["home_team"] not in teams:
        teams[row["home_team"]] = len(teams)
    if row["away_team"] not in teams:
        teams[row["away_team"]] = len(teams)

    data_row = [teams[row["home_team"]], teams[row["away_team"]], row["home_score"], row["away_score"]] + \
               [0 for _ in range(30)]

    home_series = ((df[(index + 1):]["home_team"] == row["home_team"]) |
                   (df[(index + 1):]["away_team"] == row["home_team"]))
    for i, hindex in enumerate([i for i in home_series.index if home_series[i]][:5]):
        data_row[length + i * 2:length + (i + 1) * 2] = \
            parse_result(df.at[hindex, "home_score"], df.at[hindex, "away_score"],
                         df.at[hindex, "home_team"] == row["home_team"])

    both_series = (((df[(index + 1):]["home_team"] == row["home_team"]) &
                    (df[(index + 1):]["away_team"] == row["away_team"])) |
                   ((df[(index + 1):]["home_team"] == row["away_team"]) &
                    (df[(index + 1):]["away_team"] == row["home_team"])))
    for i, hindex in enumerate([i for i in both_series.index if both_series[i]][:5]):
        data_row[length + (i + 5) * 2:length + (i + 6) * 2] = \
            parse_result(df.at[hindex, "home_score"], df.at[hindex, "away_score"],
                         df.at[hindex, "home_team"] == row["home_team"])

    away_series = ((df[(index + 1):]["home_team"] == row["away_team"]) |
                   (df[(index + 1):]["away_team"] == row["away_team"]))
    for i, hindex in enumerate([i for i in away_series.index if away_series[i]][:5]):
        data_row[length + (i + 10) * 2:length + (i + 11) * 2] = \
            parse_result(df.at[hindex, "home_score"], df.at[hindex, "away_score"],
                         df.at[hindex, "away_team"] == row["away_team"])

    return data_row


def create_dataset(path):
    # TODO: This runs in quadratic runtime, but linear is possible!
    data = ["home_team", "away_team", "home_score", "away_score"]
    dataset = pd.DataFrame(columns=data + [str(i) for i in range(30)])
    df = pd.read_csv(path)

    for index, row in df.iterrows():
        print("\r", index, "/", df.shape[0], end="")
        dataset.loc[index] = find_data_row(df, row, index, len(data))

    return dataset


def add_data(old_data_path, new_data_path, parsed_data_path, teams_path):
    old_results = pd.read_csv(old_data_path)
    new_results = pd.read_csv(new_data_path)
    parsed = pd.read_csv(parsed_data_path)
    teams = {}
    for line in open(teams_path, "r").readlines()[1:]:
        parts = line.strip().split(",")
        teams[parts[0]] = int(parts[1])
    print("Data read")

    o_index = len(old_results)
    p_index = len(parsed)
    n_index = len(new_results)

    for index, row in new_results.iterrows():
        print("\r", index, "/", n_index, end="")
        old_results.loc[o_index] = row
        o_index += 1

        parsed.loc[p_index] = find_data_row(old_results, row, teams)
        p_index += 1

    print("\nData saved")
    old_results.to_csv(old_data_path, index=False)
    parsed.to_csv(parsed_data_path, index=False)
    pd.DataFrame.from_dict(teams, orient='index', columns=['Num']).to_csv(teams_path)


if __name__ == '__main__':
    # create_dataset("./data/Goals/Goals.csv").to_csv("./data/Goals/LSP_data.csv", index=False)
    add_data("./data/Goals/results.csv", "./data/Goals/additive.csv", "./data/Goals/LSP_data_05.csv",
             "./data/Goals/LSP_teams.csv")
    '''path = "./data/Goals/results.csv"
    df = pd.read_csv(path)[::-1]
    df.drop(columns=["index"], inplace=True)
    df.to_csv(path, index=False)'''
