import pandas as pd


def parse_result(value, home_first=True):
    if home_first:
        return [int(v) for v in value.split(":")]
    return [int(v) for v in value.split(":")][::-1]


def create_dataset(path):
    data = ["Index", "Home", "Away", "Res", "Q1", "Q0", "Q2"]
    dataset = pd.DataFrame(columns=data + [str(i) for i in range(30)])
    df = pd.read_csv(path)
    for index, row in df.iterrows():
        print("\r", index, "/", df.shape[0], end="")
        data_row = [index, row["Home"], row["Away"], row["Erg"], row["Q_1"], row["Q_0"], row["Q_2"]] + \
                   [0 for _ in range(30)]

        home_series = ((df[(index + 1):]["Home"] == row["Home"]) | (df[(index + 1):]["Away"] == row["Home"]))
        for i, hindex in enumerate([i for i in home_series.index if home_series[i]][:5]):
            data_row[len(data) + i * 2:len(data) + (i + 1) * 2] = \
                parse_result(df.at[hindex, "Erg"], df.at[hindex, "Home"] == row["Home"])

        both_series = (((df[(index + 1):]["Home"] == row["Home"]) & (df[(index + 1):]["Away"] == row["Away"])) |
                       ((df[(index + 1):]["Home"] == row["Away"]) & (df[(index + 1):]["Away"] == row["Home"])))
        for i, hindex in enumerate([i for i in both_series.index if both_series[i]][:5]):
            data_row[len(data) + (i + 5) * 2:len(data) + (i + 6) * 2] = \
                parse_result(df.at[hindex, "Erg"], df.at[hindex, "Home"] == row["Home"])

        away_series = ((df[(index + 1):]["Home"] == row["Away"]) | (df[(index + 1):]["Away"] == row["Away"]))
        for i, hindex in enumerate([i for i in away_series.index if away_series[i]][:5]):
            data_row[len(data) + (i + 10) * 2:len(data) + (i + 11) * 2] = \
                parse_result(df.at[hindex, "Erg"], df.at[hindex, "Away"] == row["Away"])

        dataset.loc[index] = data_row

    return dataset


create_dataset("./data/Quotes/BL.csv").to_csv("./data/Quotes/BuLi_data.csv", index=False)
