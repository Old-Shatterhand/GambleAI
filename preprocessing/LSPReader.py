import pandas as pd


def read():
    df = pd.read_csv("./data/Goals/results.csv")
    df.drop(df.columns[-4:], inplace=True, axis=1)
    # df.sort_values(by="date", ascending=False, inplace=True)
    return df


def encode_teams(df):
    teams = {}
    for index, row in df.iterrows():
        if row["home_team"] not in teams:
            teams[row["home_team"]] = len(teams)
        if row["away_team"] not in teams:
            teams[row["away_team"]] = len(teams)

        df.loc[index, "home_team"] = teams[row["home_team"]]
        df.loc[index, "away_team"] = teams[row["away_team"]]
    return df.sort_values(by="date", ascending=False), teams


df, table = encode_teams(read())
df.to_csv("./data/Goals/Goals.csv", index=False)
pd.DataFrame.from_dict(table, orient='index', columns=['Num']).to_csv("./data/Goals/LSP_teams.csv")
