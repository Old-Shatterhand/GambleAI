import pandas as pd


def extract_tournament(name, start_day, start_home_team, end_day, end_home_team):
    data = pd.read_csv("./data/Goals/LSP_data_05.csv")
    results = pd.read_csv("./data/Goals/results.csv")
    start = results[(results.date == start_day) & (results.home_team == start_home_team)].first_valid_index()
    end = results[(results.date == end_day) & (results.home_team == end_home_team)].first_valid_index() + 1
    start_split, end_split = len(data) - (len(results) - start), len(data) - (len(results) - end)
    train_data = data[:start_split]
    test_data = data[start_split:end_split]
    test_data = test_data[list((results[start:end].tournament == name).array)]
    return train_data, test_data


if __name__ == '__main__':
    # TODO: Last day for tournament
    euro_train, euro_test = extract_tournament("UEFA Euro", "2021-06-11", "Italy", "2021-07-11", "Italy")  # extract_euro_2021()
    copa_train, copa_test = extract_tournament("Copa América", "2021-06-17", "Colombia", "2021-07-10", "Argentina")  # extract_copa_2021()
    euro_train.to_csv("./challenges/EURO2020/train.csv")
    euro_test.to_csv("./challenges/EURO2020/test.csv")
    copa_train.to_csv("./challenges/COPA2021/train.csv")
    copa_test.to_csv("./challenges/COPA2021/test.csv")
