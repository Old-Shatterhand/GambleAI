import pandas as pd


def read_10_19():
    df = pd.read_excel("./data/Quotes/Odds-Analyse-6-Ligen.xls", "GER 1", converters={"Datum": str})
    drop = [x for x in df.columns if x not in ["Sp", "Datum", "Heim", "Gast", "Erg", "Toto", "Q_1", "Q_0", "Q_2"]]
    df.drop(drop, inplace=True, axis=1)
    df.rename(columns={"Datum": "Date", "Heim": "Home", "Gast": "Away"}, inplace=True)
    df = df[df["Sp"].notna()]
    season_list = []
    last_sp = 13
    season = 20
    for index, row in df.iterrows():
        if row.Sp > last_sp:
            season -= 1
        season_list.append(season)
        last_sp = row.Sp
    df.insert(0, "Season", season_list, True)
    df = df.astype({"Season": str})
    df["Date"] = [s.split(" ")[0] for s in df["Date"]]
    return df


def read_04_09(season):
    next_y = str(int(season) + 1)
    df = pd.read_excel("./data/Quotes/Bf-Odds-Deu-1.xls", season + ("-0" if len(next_y) == 1 else "-") + next_y,
                       converters={"Date": str})
    drop = [x for x in df.columns if x not in ["Sp", "Date", "Home", "Away", "Erg", "Toto", "Q_1", "Q_0", "Q_2"]]
    df.drop(drop, inplace=True, axis=1)
    df.insert(0, "Season", season, True)
    df["Date"] = [s.split(" ")[0] for s in df["Date"]]
    return df


def merge_data():
    df = pd.concat([read_04_09(s) for s in ["04", "05", "06", "07", "08", "09"]] + [read_10_19()])
    df.sort_values(by=["Date"], inplace=True)
    df.to_excel("./data/Quotes/BL_complete.xls", index=False)


def encode_teams(df):
    teams = {}
    duplicates = {"Dortmund": "BVB 09 Dortmund", "Kaiserslautern": "FC Kaiserslautern",
                  "Leverkusen": "Bayer Leverkusen", "Herta BSC": "Herta BSC Berlin",
                  "Arm. Bielefeld": "Arminia Bielefeld", "M'gladbach": "Mönchengladbach",
                  "FSV Mainz": "1. FSV Mainz 05", "Frankfurt": "Eintracht Frankfurt",
                  "Hansa Rostock": "FC Hansa Rostock", "Werder Bremen": "SV Werder Bremen"}
    for index, row in df.iterrows():
        row["Home"] = duplicates.get(row["Home"], row["Home"])
        row["Away"] = duplicates.get(row["Away"], row["Away"])

        if row["Home"] not in teams:
            teams[row["Home"]] = len(teams)
        if row["Away"] not in teams:
            teams[row["Away"]] = len(teams)

        df.loc[index, "Home"] = teams[row["Home"]]
        df.loc[index, "Away"] = teams[row["Away"]]
    return df.sort_values(by="Date", ascending=False), teams


df, table = encode_teams(pd.read_excel("./data/Quotes/BL_complete.xls"))
df.to_csv("./data/Quotes/BL.csv", index=False)
pd.DataFrame.from_dict(table, orient='index', columns=['Num']).to_csv("./data/Quotes/BL_teams.csv")
