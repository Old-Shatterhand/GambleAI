# Challenges
In this folder, I collect challenges. All these challenges have in common that a model has to be fit to predict results 
of football (soccer) matches. Each challenge focusses on a single tournament like the EURO 2020 or the COPA 2021. 
The detailed descriptions of the tasks are given in the according folders. All challenges are presented in the 
following with a short summary and the top-3 of the leader board for the respective challenge.

Some notes on what the challenges have in common:
- Each entry in the datasets contains ...
  - ... the numeric encodings of both teams
  - ... the match result in goals
  - ... the last 5 results of the hometeam, the awayteam and the direct matches
> At a later point, the datasetsize will be extended a bit to take the last 10 or 20 games into account. The according 
> variable will be called K in the following.
- All goals are cut off at 4. This means if a team scores 4 or more goals this only counts as 4. This is to not predict 
  too many possible outcomes and make the problem still applicable for classification approaches without too much 
  classes to distinguish.
> Maybe, at a later point, this will also be discarded
- The predictions are evaluated after the regular time. No extra time, no shootouts. So, also knockout-games can end up 
  in a draw.

More details can be found after the challenges.

## EURO 2020
You are given the results of football matches between any nations since 2005. The results are evaluated using the 
KickTipp-Score (described below). This tournament had 51 matches, so the maximal score is 204. Further metrics are the 
number 

| Author | Method | R/C | Teams | K | Score | Res | Trend | Win | Wrong |
|--------|--------|-----|-------|---|-------|-----|-------|-----|-------|
| Old-Shatterhand | Linear Regression | C | No | 5 | 64 | 5.88% | 7.84% | 39.22% | 66.67% |
| Old-Shatterhand | Neural Network | C | No | 5 | 56 | 5.88% | 11.76% | 25.49% | 56.86% |
| Old-Shatterhand | Random Forrest | C | No | 5 | 42 | 5.88% | 3.92% | 23.53% | 47.06% |
| - | - | - | - | - | - | - | - | - | - |

## COPA 2021
You are given the results of football matches between any nations since 2005. The results are evaluated using the 
KickTipp-Score (described below). This tournament had 28 matches, so the maximal score is 112. Further metrics are the 
number 

| Author | Method | R/C | Teams | K | Score | Res | Trend | Win | Wrong |
|--------|--------|-----|-------|---|-------|-----|-------|-----|-------|
| Old-Shatterhand | Random Forrest | C | No | 5 | 32 | 16.67% | 0.00% | 33.33% | 50.00% |
| Old-Shatterhand | Linear Regression | C | No | 5 | 32 | 8.33% | 0.00% | 50.00% | 41.67% |
| Old-Shatterhand | Neural Network | C | No | 5 | 18 | 0.00% | 8.33% | 25.00% | 66.67% |
| - | - | - | - | - | - | - | - | - | - |
| - | - | - | - | - | - | - | - | - | - |

# KickTipp-Score
This is a score used by the german [KickTipp-Website](https://www.kicktipp.de/). It assigns points to the guesses of 
players based on the true result of the game as follows:
- 4 points: True result was guessed.
- 3 points: The game had a winner and the guess had the right tendency, i.e. true: 2-1, guess 3-2.
- 2 points: The game ended in a draw and the player had not the correct draw