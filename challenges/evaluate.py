import pandas as pd

from classification.goal_for import GoalForrestModel
from classification.goal_net import GoalNetModel
from classification.goal_reg import GoalRegModel
from utils.loss import KickTippLoss


def evaluate(model, challenge):
    data = pd.read_csv(F"./{challenge}/test.csv").to_numpy()
    results, input_results = data[:, -32:-30], data[:, -30:]

    loss, scores = KickTippLoss()(model.predict(input_results), results, stats=True)
    scores = [loss] + scores

    return [s / n for s, n in zip(scores, [1, len(results), len(results), len(results), len(results)])]


if __name__ == '__main__':
    # challenge = "EURO2020"
    challenge = "COPA2021"

    gfm = GoalForrestModel(None).load(F"./{challenge}/models/gfm.pkl")
    print("GoalForrestModel:\n\t", evaluate(gfm, challenge))

    gfm = GoalNetModel(None)
    gfm.load(F"./{challenge}/models/gnm.pth")
    print("GoalNetworkModel:\n\t", evaluate(gfm, challenge))

    gfm = GoalRegModel().load(F"./{challenge}/models/grm.pkl")
    print("GoalRegressionModel:\n\t", evaluate(gfm, challenge))
