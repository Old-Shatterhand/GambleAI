import os

import torch
from ray import tune
from torch import optim
from torch.utils import data

from utils.model import Model
from utils.loss import QuoteLoss
from utils.net import QuoteNet


class QuoteNetModel(Model):
    def __init__(self, config):
        if any(x not in config for x in ["hidden", "goals", "mode", "weight", "max_value", "lr", "bs", "episodes",
                                         "tune", "path"]):
            raise ValueError("Not all fields in config defined.")

        super(QuoteNetModel, self).__init__(config)
        self.net = QuoteNet(hidden_size=config["hidden"], goals=config["goals"], mode=config["mode"])
        self.loss = QuoteLoss(weight=config["weight"], max_value=config["max_value"])
        self.optimizer = optim.Adam(self.net.parameters(), lr=config["lr"])
        if config["path"] is not None:
            self.net.load(config["path"])

    def fit(self, train_data, valid_data=None):
        loader = data.DataLoader(train_data, batch_size=self.config["bs"], shuffle=True)

        # some general statistics
        episode_losses, episode_wins = [], []

        for e in range(self.config["episodes"]):

            # some episode statistics
            episode_loss, episode_win, i = 0, 0, 1

            for i, (value, label) in enumerate(loader):
                # forwarding the data through the network and compute the loss
                output = self.net.forward(value)
                loss = self.loss(output, label)

                # backpropagation
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()

                # do some statistics
                loss = loss.item()
                episode_loss += loss
                episode_win -= loss

                if not self.config["tune"]:
                    print("\rEpisode", e + 1, "/", self.config["episodes"], "- Batch", i + 1, "/",
                          len(loader) // self.config["bs"], "\tLoss: %.5f" % loss, end="")

            episode_losses.append(episode_loss / i)
            episode_wins.append(episode_win / i)

            validation = self.__valid(valid_data)
            if self.config["tune"]:
                with tune.checkpoint_dir(e) as checkpoint_dir:
                    self.net.save(os.path.join(checkpoint_dir, "checkpoint"))
                tune.report(loss=episode_losses[-1], accuracy=validation)
            else:
                print("\rEpisode", e + 1, "/", self.config["episodes"], "- Completed \tLoss: %.5f" % episode_losses[-1],
                      "\tValidation-Diff: %.2f" % validation)

        return episode_losses, episode_wins

    def test(self, test_data):
        return self.__valid(test_data)

    def predict(self, test_data):
        with torch.no_grad:
            return self.net.forward(test_data)

    def save(self, filename):
        self.net.save(filename)

    def load(self, filename):
        self.net.load(filename)

    def __valid(self, valid_data):
        loader = data.DataLoader(valid_data, batch_size=self.config["bs"], shuffle=True)
        validation = 0
        with torch.no_grad():
            for value, label in loader:
                validation -= self.loss(self.net.forward(value), label).item()
        return validation / len(loader)
