import torch
from sklearn.linear_model import LinearRegression

from utils.model import Model
from utils.loss import GoalLoss


class GoalRegModel(Model):
    def __init__(self):
        super(GoalRegModel, self).__init__(None)
        self.model = LinearRegression()

    def fit(self, train_data, valid_data=None):
        train_x, train_y = train_data
        self.model.fit(train_x, train_y)

    def test(self, test_data):
        return self.__valid(test_data)

    def predict(self, test_data):
        return self.model.predict(test_data)

    def predict_result(self, match_data):
        guess = self.model.predict(match_data)[0]
        return guess[:5].argmax(), guess[5:].argmax()

    def __valid(self, valid_data):
        valid_x, valid_y = valid_data
        return GoalLoss()(torch.tensor(self.model.predict(valid_x)),
                          torch.tensor(valid_y)).sum().item() / (len(valid_y) * 2)
