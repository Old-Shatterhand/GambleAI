import numpy as np
import torch
from sklearn.ensemble import RandomForestClassifier as RFC

from utils.loss import GoalLoss
from utils.model import Model


class GoalForrestModel(Model):
    def __init__(self, config):
        super(GoalForrestModel, self).__init__(config)
        self.home_model = RFC()
        self.away_model = RFC()

    def fit(self, train_data, valid_data=None):
        train_x, train_y = train_data
        self.home_model.fit(train_x, np.argmax(train_y[0], axis=1))
        self.away_model.fit(train_x, np.argmax(train_y[1], axis=1))

    def test(self, test_data):
        return self.__valid(test_data)

    def predict(self, test_data):
        return list(zip(*(self.home_model.predict_proba(test_data), self.away_model.predict_proba(test_data))))

    def predict_result(self, match_data):
        return self.home_model.predict_proba(match_data).argmax(), self.away_model.predict_proba(match_data).argmax()

    def __valid(self, valid_data):
        valid_x, valid_y = valid_data
        return torch.sum(GoalLoss()(self.home_model.predict_proba(valid_x), valid_y[0]) +
                         GoalLoss()(self.away_model.predict_proba(valid_x), valid_y[1])).item() / (max(valid_y[0].
                                                                                                       shape) * 2)
