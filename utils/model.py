import pickle


class Model:
    def __init__(self, config):
        self.config = config

    def fit(self, train_data, valid_data=None):
        pass

    def test(self, test_data):
        return self.__valid(test_data)

    def predict(self, test_data):
        pass

    def predict_result(self, match_data):
        pass

    def save(self, filename):
        pickle.dump(self, open(filename, "wb"))

    def load(self, filename):
        """
        Load model from pickled file.
        Usage:
            model.save("./models/gfm.pkl")
            model = load("./models/gfm.pkl")

        :param filename:
        :return:
        """
        return pickle.load(open(filename, "rb"))

    def __valid(self, valid_data):
        pass
