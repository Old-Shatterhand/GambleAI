import torch


class QuoteLoss:
    def __init__(self, weight=1, max_value=1):
        self.weight = weight
        self.max_value = max_value

    def __call__(self, *args, **kwargs):
        output = args[0]
        label = args[1]
        win = torch.stack([output[j, int(l.item())] * label[j, int(l.item())]
                           for j, l in list(enumerate(label[:, -1]))])
        true_win = win - self.weight * (torch.sum(output[:, :-1], dim=1) +
                                        torch.stack([output[j, int(l.item())]
                                                     for j, l in list(enumerate(label[:, -1]))]))
        return -torch.sum(true_win)


class GoalLoss:
    def __init__(self):
        pass

    def __call__(self, *args, **kwargs):
        output = args[0]
        if not isinstance(output, torch.Tensor):
            output = torch.tensor(output)

        label = args[1]
        if not isinstance(label, torch.Tensor):
            label = torch.tensor(label)

        if output.shape[1] == 10:
            return torch.abs(torch.argmax(output[:, :5], dim=1) - torch.argmax(label[:, :5], dim=1)) + \
                   torch.abs(torch.argmax(output[:, 5:], dim=1) - torch.argmax(label[:, 5:], dim=1))
        else:
            return torch.abs(torch.argmax(output, dim=1) - torch.argmax(label, dim=1))


class KickTippLoss:
    def __init__(self):
        pass

    def __call__(self, *args, **kwargs):
        output = args[0]
        if not isinstance(output, torch.Tensor):
            output = torch.tensor(output)

        label = args[1]
        if not isinstance(label, torch.Tensor):
            label = torch.tensor(label)

        if output.shape[1] == 10:
            output = torch.stack([torch.argmax(output[:, :5], dim=1), torch.argmax(output[:, 5:], dim=1)], dim=1)
        elif output.shape[1] == 2:
            output = torch.stack([torch.argmax(output[:, 0, :], dim=1), torch.argmax(output[:, 1, :], dim=1)], dim=1)
        elif output.shape[1] == 25:
            pass

        loss = 0
        stats = [0, 0, 0, 0]
        for out, lab in zip(output, label):
            if out[0] == lab[0] and out[1] == lab[1]:
                loss += 4
                stats = [stats[0] + 1] + stats[1:]
            elif out[0] - out[1] == lab[0] - lab[1]:
                if out[0] == out[1]:
                    loss += 2
                    stats = stats[:2] + [stats[2] + 1, stats[3]]
                else:
                    loss += 3
                    stats = [stats[0], stats[1] + 1] + stats[2:]
            elif (out[0] - out[1]) * (lab[0] - lab[1]) > 0:
                loss += 2
                stats = stats[:2] + [stats[2] + 1, stats[3]]
            else:
                stats = stats[:3] + [stats[3] + 1]

        return_val = []

        if kwargs.get("optim", "max") == "min":
            return_val.append(output.shape[0] / loss)
        else:
            return_val.append(loss)

        if kwargs.get("stats", False):
            return_val.append(stats)
        else:
            return_val = return_val[0]

        return return_val
