import numpy as np
import pandas as pd
import torch
from torch.utils import data


class Set(data.Dataset):
    def __init__(self, path="", df=None, shuffle=True, classification=True):
        if df is None:
            self.df = pd.read_csv(path)
        else:
            self.df = df
        if shuffle:
            self.df = self.df.iloc[np.random.permutation(len(self.df))]
            self.df.reset_index(drop=True, inplace=True)
        self.classification = classification

    def __len__(self):
        return self.df.shape[0]

    def __getitem__(self, item):
        pass

    def get_arrays(self, **kwargs):
        pass

    def get_splits(self, set, split=(0.7, 0.9)):
        train_split, valid_split, test_split = [], [], []
        max_train, max_valid = split[0] * len(self.df), split[1] * len(self.df)
        for i in range(len(self.df)):
            if i < max_train:
                train_split.append(True)
                valid_split.append(False)
                test_split.append(False)
            elif i < max_valid:
                train_split.append(False)
                valid_split.append(True)
                test_split.append(False)
            else:
                train_split.append(False)
                valid_split.append(False)
                test_split.append(True)
        return set(df=self.df[train_split]), set(df=self.df[valid_split]), set(df=self.df[test_split])


class QuoteSet(Set):
    def __init__(self, path="", df=None, shuffle=True):
        super(QuoteSet, self).__init__(path, df, shuffle)

    def __getitem__(self, item):
        row = self.df.loc[item].tolist()
        res = [int(i) for i in row[3].split(":")]
        values = row[7:]
        if self.classification:
            output = row[4:7]
            if res[0] < res[1]:
                output += [2]
            elif res[0] > res[1]:
                output += [0]
            elif res[0] == res[1]:
                output += [1]
        else:
            output = 0
            if res[0] < res[1]:
                output = -1
            elif res[0] > res[1]:
                output = 1
        return torch.Tensor(values), torch.Tensor(output)

    def get_arrays(self, **kwargs):
        tmp = self.df.to_numpy()
        x = tmp[:, 7:]
        if self.classification:
            y = np.zeros((len(tmp), 4))
            y[:, :3] = tmp[:, 4:7]
            y[y[:, 0] == y[:, 1], 3] = 1
            y[y[:, 0] < y[:, 1], 3] = 2
        else:
            y = np.zeros(len(tmp))
            # TODO: Implement labeling according to scored goals
        return x, y

    def split(self, split=(0.7, 0.9)):
        return super().get_splits(QuoteSet, split)


class GoalSet(Set):
    def __init__(self, path="", df=None, shuffle=True, classification=True):
        super(GoalSet, self).__init__(path, df, shuffle, classification)

    def __getitem__(self, item):
        row = self.df.loc[item].tolist()

        if self.classification:
            label = [torch.zeros(5), torch.zeros(5)]
            label[0][min(row[3], 4)] = 1
            label[1][min(row[4], 4)] = 1
            label = torch.cat(label)
        else:
            label = torch.min(torch.tensor(row[3:5]), 4)

        values = row[5:]
        return torch.Tensor(values), label

    def get_arrays(self, **kwargs):
        tmp = self.df.to_numpy()
        x = tmp[:, 5:]
        if self.classification:
            y = (np.zeros((len(x), 5)), np.zeros((len(x), 5)))
            for i, (c, d) in enumerate(zip(np.minimum(tmp[:, 3], np.full_like(tmp[:, 4], 4)), np.minimum(tmp[:, 4], np.full_like(tmp[:, 4], 4)))):
                y[0][i, c] = 1
                y[1][i, d] = 1
            if kwargs.get("classes", False):
                y = np.concatenate(y, axis=1)
        else:
            y = tmp[:, 3:5]
        return x, y

    def split(self, split=(0.7, 0.9)):
        return super().get_splits(GoalSet, split)
