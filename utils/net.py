import torch
from torch import nn


class Net(nn.Module):
    def __init__(self, hidden_size=64, goals=False, mode="big", output=4):
        super(Net, self).__init__()
        self.goals = goals
        self.fcg = nn.Sequential(
            nn.Linear(2, 2)
        )
        self.big_net = nn.Sequential(
            nn.Linear(30, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, hidden_size // 2),
            nn.ReLU(),
            nn.Linear(hidden_size // 2, output)
        )
        self.tiny_net = nn.Sequential(
            nn.Linear(30, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, output)
        )
        if mode == "big":
            self.net = self.big_net
        else:
            self.net = self.tiny_net

    def forward(self, x):
        if self.goals:
            x = torch.cat([self.fcg(x[:, i:i + 2]) for i in range(0, x.shape[1], 2)], dim=1)

        if not isinstance(x, torch.Tensor):
            x = torch.tensor(x).float()

        return self.net.forward(x)

    def save(self, path):
        torch.save(self.state_dict(), path)

    def load(self, path):
        self.load_state_dict(torch.load(path))


class QuoteNet(Net):
    def __init__(self, hidden_size=52, goals=False, mode="big"):
        super(QuoteNet, self).__init__(hidden_size=hidden_size, goals=goals, mode=mode, output=4)

    def forward(self, x):
        x = super().forward(x)
        x = nn.Softmax(dim=0)(x)
        return x


class GoalNet(Net):
    def __init__(self, hidden_size=52, goals=False, mode="big"):
        super(GoalNet, self).__init__(hidden_size=hidden_size, goals=goals, mode=mode, output=2)

    def forward(self, x):
        x = super().forward(x)
        x = nn.ReLU()(x)
        return x


class GoalNet2(Net):
    def __init__(self, hidden_size=52, goals=False, mode="big"):
        super(GoalNet2, self).__init__(hidden_size=hidden_size, goals=goals, mode=mode, output=10)

    def forward(self, x):
        x = super().forward(x)
        x1, x2 = x[:5], x[5:]
        x1 = nn.Softmax(dim=1)(x1)
        x2 = nn.Softmax(dim=1)(x2)
        return torch.cat([x1, x2])
